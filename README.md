# VGA TEST #

This is a test of VGA drawing using mode 13h (320x200 265 colors)

![HOLY FUCKING SHIT IT'S A BOX IN DOS MADE IN ASM OHH MY GOOOOOOOOD.png](https://bitbucket.org/repo/zAgyoA/images/534343263-HOLY%20FUCKING%20SHIT%20IT%27S%20A%20BOX%20IN%20DOS%20MADE%20IN%20ASM%20OHH%20MY%20GOOOOOOOOD.png)

### Why? ###

* Hell if I know.
* To learn Assembly, possibly.
* For fun!
* Was bored...


### How to use it? ###

* Either download already built program, and put it into DOSBOX, or MS-DOS, Win 3.11, Win95, Win98.
* Get MASM, build it, and do step 1.

### Who made it? ###

* Well, me, RedPanda aka Dmytro Kalchenko
* My friend who's better in asm than me, yuraSniper aka Yuriy Stets