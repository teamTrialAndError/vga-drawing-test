.286
.model small, stdcall
.stack 100h

; 08E30h - 9FFF0h (MOST LIKELY) FREE MEM

.data
	frontBuffer dw 0A000h
	backBuffer dw 0B000h
	
	boxX dw 0
	boxY dw 0
	boxW dw 5
	boxH dw 5
	boxC dw 10
	
.code
	start:
	
	call Init
	call Main
	call CleanUp
	
	Main proc

		mov ax, [backBuffer]
		mov es, ax
		
		push 1
		call Clear
		
		push [boxX]
		push [boxY]
		push [boxW]
		push [boxH]
		push [boxC]
		call DrawBox
		
		mov ax, [frontBuffer]
		mov es, ax
		
		mov di, 0
		mov si, 0
		
		mov cx, (320 * 200) / 2
		
		push ds
		
		mov ax, [backBuffer]
		mov ds, ax
		
		call memcpy
		
		pop ds
		
		inc [boxX]
		cmp [boxX], 200
		jnz Main
		
		ret
	Main EndP

	; copies mem from DS:SI to ES:DI CX bytes
	memcpy proc
		rep movsw
		
		ret
	memcpy EndP

	Clear proc col0:WORD
	
		xor di, di
		mov cx, (320 * 200) / 2
		
		mov al, BYTE PTR [col0]
		mov ah, al
		
		rep stosw
	
		ret
	Clear EndP
	
	Cls proc
		mov ah, 0h 
		mov al, 13h
		int 10h
		
		ret
	Cls EndP

	DrawBox proc col0:WORD, ht0:WORD, wd0:WORD, y0:WORD, x0:WORD
		mov ax, [x0]
		add ax, [wd0]
		mov [wd0], ax
		
		mov ax, [y0]
		add ax, [ht0]
		mov [ht0], ax
		
		mov bx, [x0]
		mov cx, [y0]
		mov al, BYTE PTR [col0]
		
		.while bx < [wd0]
			.while cx < [ht0]
				call DrawPixel
				inc cx
			.endw
			mov cx, [y0]
			inc bx
		.endw
		
		ret
	DrawBox EndP

	DrawPixel proc
		push ax
		
		mov ax, cx
		mov dx, 320
		mul dx
		add ax, bx
	
		mov di, ax;
		
		pop ax
		stosb
		
		ret
	DrawPixel EndP
	
	Init proc
		mov ax, @data
		mov ds, ax
		
		mov ah, 0h 
		mov al, 13h
		int 10h
		
		cld
		
		ret
	Init EndP
	
	CleanUp proc
		mov ah, 1h
		int 21h
		
		mov ah, 0h 
		mov al, 3h
		int 10h
	
		mov ah, 4ch
		int 21h
		
		ret
	CleanUp EndP
end start